'use strict';
import { Physics } from './physics.mjs';
//const express = require('express');
import express from 'express';
// Constants
const PORT = 8090;
const HOST = '0.0.0.0';

// App
const app = express();
app.use(express.static('../html'));
app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);


const physics = new Physics();

import  server  from 'websocket';

const WebSocketServer = server.server;
import http from 'http';
import { exit } from 'process';


const wsserver = http.createServer(function(request, response) {
});
wsserver.listen(3773, function() { });


class ServerObject{
  constructor(id, size, x, y){
    this.id = id;
    this.size =size;
    this.x = x;
    this.y = y;
    this.speedX = 0;
    this.speedY = 0;
  }

  move(x, y) {
    this.x = x;
    this.y = y;
  }

  getJSON() {
    return JSON.stringify({id:this.id, size:this.size, x:this.x, y:this.y, size:this.size });
  }

  toJSON() {
    return {id:this.id, size:this.size, x:this.x, y:this.y, size:this.size };
  }
}

// create the server
const wsServer = new WebSocketServer({
  httpServer: wsserver
});
let clientId = 0;
const maxclients = 4;
let socket;

// WebSocket server
wsServer.on('request', function(request) {
  const id = clientId;
  var connection = request.accept(null, request.origin);
  connection.on('close', function(connection) {
    console.log("bye "+id);
    physics.physicsClose(id)
  });  

  physics.connectClient(connection, clientId);
  
  clientId+=1;
  if(clientId >=maxclients) clientId = 0;
  
});
