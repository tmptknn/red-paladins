import {Physics} from './physics.mjs';

const canvas = document.getElementById('canvas');

const localServerCheckBox = document.getElementById('localserver');
let isLocal = localServerCheckBox.checked;
let physics = null;
localServerCheckBox.onclick = () =>{
  isLocal = localServerCheckBox.checked;
}

const ctx = canvas.getContext('2d');
function localPhysicsMove(message){
  physics.physicsMove({utf8Data:message, type:"utf8"});
}

const realWidth = 0.3048;
const realHeight = 0.6096;

const ratio = realHeight/canvas.height;

class GameObject{
  constructor(id, size, x, y, color){
    this.id = id;
    this.size =size;
    this.x = x;
    this.y = y;
    this.color = color;
  }

  

  move(socket, x, y){
    this.localMove(x,y);
    //localPhysicsMove(x,y);
    this.networkMove(socket);
  }

  localMove(x,y){
    this.x = x;
    this.y = y;
  }

  networkMove(socket){
    if(isLocal){
      localPhysicsMove(this.getJSON());
    }else{
      socket.send(this.getJSON());
    }
  }

  getJSON() {
    return JSON.stringify({id:this.id, size:this.size, x:this.y*ratio, y:(canvas.width-this.x)*ratio, size:this.size*ratio });
  }

  draw(ctx) {
    ctx.fillStyle = this.color;
    ctx.beginPath();
    ctx.arc(this.x, this.y, this.size, 0, 2*Math.PI);
    ctx.fill();
  }
}

const objects = [];
const paddle = new GameObject(-1,0.0381/ratio,canvas.width/2,canvas.height/2,"#FFFFFF99")
let socket;
const maxclients = 4;
let score =[0,0];

for(let i=0; i< maxclients; i++){
  objects[i] = new GameObject(i,0.0381/ratio,canvas.width/2,canvas.height/2,(i%2===0)?"red":"blue");
}
objects[maxclients] = new GameObject(maxclients,0.01905/ratio,canvas.width/2,canvas.height/2, "white");
objects[objects.length] = new GameObject(objects.length,0.0381/ratio,canvas.width/3,canvas.height/3, "green");
let myId = 0;

function mouseMove(e){
  // console.log("event "+e.pageX+" "+e.pageY);
  paddle.move(socket, e.pageX, e.pageY);
}

function touchMove(event) {
  const x = (event.touches[0].clientX - canvas.offsetLeft);
  const y = (event.touches[0].clientY - canvas.offsetTop);
  mouseMove({pageX:x,pageY:y})
}

function refresh(){
  ctx.fillStyle = "lightGreen";
  ctx.fillRect(0,0,canvas.width,canvas.height);

  ctx.fillStyle = "black";

  ctx.fillRect(canvas.width/4,0,canvas.width/2,10);
  ctx.fillRect(canvas.width/4,canvas.height-10,canvas.width/2,canvas.height);

  for(let i=0; i<=maxclients+1; i++){
    if(objects[1]) objects[i].draw(ctx);
  }
  paddle.draw(ctx);
  ctx.fillStyle = "white";
  ctx.font = "40px Arial";
  ctx.fillText(""+score[1],canvas.width/2,canvas.height/4);
  ctx.fillText(""+score[0],canvas.width/2,canvas.height*3/4);
  requestAnimationFrame(refresh);
}

canvas.addEventListener("mousemove",mouseMove,true);

document.body.addEventListener('touchmove', (e) => { // eslint-disable-line no-undef
        if (e.target === canvas) {
          e.stopPropagation();
          e.preventDefault();
        }
      }, false);

canvas.addEventListener('touchmove', touchMove,false);

function onUTF(message) {

   let json;
   //console.log(""+message.data);
   try {
   json = JSON.parse(message.data);
   } catch (e) {
     console.log('This doesn\'t look like a valid JSON: ',
         message.data);
     return;
   }
   if(json){
     if(json.yourId !== undefined){
       console.log("got id "+json.yourId);
       myId = json.yourId;
       paddle.id=myId;
     } else if(json.id !== undefined){
       objects[json.id].localMove((realWidth-json.y)/ratio, json.x/ratio);
     } else if(json.team0Score !== undefined){
        score[0] = json.team0Score;
        score[1] = json.team1Score;
        console.log("Score !!");
     } else {
       console.log(""+message.data);
     }
   }
};

function onLocalUTF(message){
  if(isLocal){
    onUTF({data:message});
  }
}

if(isLocal){
  physics = new Physics();
  physics.addClient(onLocalUTF, myId);
}
else{
  ////?
}
refresh();
if(!isLocal){
  const ho = location.host.split(":")[0];

  socket = new WebSocket("ws://"+(ho?ho:"localhost")+":3773");

  socket.onopen = function() {
    console.log("connection open");
  };

  socket.onmessage = onUTF;

  socket.onclose = function() {

    // websocket is closed.
    console.log("Connection is closed...");
  };

}
