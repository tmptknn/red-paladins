import {ai} from './ai.mjs';

class ServerObject{
    constructor(id, size, x, y, type, side){
      this.id = id;
      this.size =size;
      this.x = x;
      this.y = y;
      this.speedX = 0;
      this.speedY = 0;
      this.type = type;
      this.side = side;
    }
  
    move(x, y, width, height) {
      let mx = x;
      if(width && height){
        switch(this.side){
          case 1:
            if(x>width/2){
              mx=width/2;
            }
            break;
          case 0:
            if(x<width/2){
              mx=width/2;
            }
            break;
          default:

            break;
        }
      }
      this.x = mx;
      this.y = y;

    }
  
    getJSON() {
      return JSON.stringify({id:this.id, size:this.size, x:this.x, y:this.y, size:this.size });
    }
  
    toJSON() {
      return {id:this.id, size:this.size, x:this.x, y:this.y, size:this.size };
    }
  }


  class Physics{

    constructor(){
      this.clients = [];
      this.maxclients = 4;
      this.width = 0.6096;
      this.height = 0.3048;
      
      this.objects = [];
      this.puck;
      
      for(let i=0; i< this.maxclients; i++){
        this.objects[i] = new ServerObject(i,0.0381,-0.0381,-0.0381,"player",i%2);
      }
      this.objects[this.maxclients] = this.puck = new ServerObject(this.maxclients,0.01905,this.width*3/4,this.height/2,"puck",-1);
      this.objects[this.objects.length] = new ServerObject(this.objects.length,0.0381,this.width/3,this.height/3,"aiplayer",1); //ai kiekko
      this.damping = 0.01;
      this.scoreRed = 0;
      this.scoreBlue = 0;
      const that = this;
      this.AI = new ai(this.objects,this.objects.length-1,this.width,this.height, (object)=>{that.move(object)});
      this.sendScore();
      setInterval(()=>{ that.update() },30);
    }
  
    sendScore(){
    const data = JSON.stringify({team0Score:this.scoreBlue, team1Score:this.scoreRed });
    for(let i=0; i<this.maxclients; i++) {
      if(this.clients[i]) this.clients[i].connection.sendUTF(data);
    }
  }
  
  goal(team) {
    this.sendScore();
    this.puck.speedX = 0;
    this.puck.speedY = 0;
    this.puck.x = this.width*(team === 1?3:1)/4+(Math.random()-0.5)*this.puck.size;
    this.puck.y = this.height/2+(Math.random()-0.5)*this.puck.size;
  }
  
  updateScore() {
    if(this.puck.x< 0){
      this.scoreBlue+=1;
      this.goal(0);
    }
    if(this.puck.x> this.width){
      this.scoreRed+=1;
      this.goal(1);
     }
  }
  
  update(){
    let dx = this.puck.speedX*(1.0 - this.damping);
    let dy = this.puck.speedY*(1.0 - this.damping);
    for(let i =0; i< this.maxclients+2; i++){
      if(i != this.maxclients){
        const o = this.objects[i];
        const xd = (this.puck.x+dx)-o.x;
        const yd = (this.puck.y+dy)-o.y;
        const len =Math.sqrt(xd*xd+yd*yd);
        if(len<this.puck.size+o.size) {
          const inside = (this.puck.size+o.size)-len;
          const nn = xd*xd+yd*yd;
          const vn = xd*dx+yd*dy;
          if(vn <= 0){
            dx -= (2*(vn/nn))*xd;
            dy -= (2*(vn/nn))*yd;
          }
          dx += xd*inside/(len*2);
          dy += yd*inside/(len*2);
        }
      }
    }
  
    if((this.puck.x+dx < this.puck.size ||
      this.puck.x+dx > this.width - this.puck.size)
        && (this.puck.y + dy < this.height/4 -this.puck.size ||
          this.puck.y + dx > this.height*3/4+this.puck.size)) {
      dx = -dx
    }
  
    if(this.puck.y+dy < this.puck.size || this.puck.y+dy > this.height - this.puck.size){
      dy = -dy
    }
  
    this.puck.speedX = dx;
    this.puck.speedY = dy;
  
    this.puck.move(this.puck.x+this.puck.speedX, this.puck.y+this.puck.speedY, this.width, this.height);
  
    this.updateScore();
    this.move(this.puck.toJSON());
  }
  
  move(object){
    if(object.id < this.maxclients && object.id >=0){
      this.objects[object.id].move(object.x, object.y, this.width, this.height);
    }
    if(object.id>=0){
      const data = JSON.stringify(this.objects[object.id]);
    

      for(let i=0; i<this.maxclients; i++) {
        if(this.clients[i]) this.clients[i].connection.sendUTF(data);
      }
    }
  }
  
  addClient(sendUTFCallback, clientId){
    const id = clientId;
    this.clients[clientId] = {connection:{sendUTF:sendUTFCallback}};
    this.clients[clientId].connection.sendUTF(JSON.stringify({yourId:id}));
    for(let i=0; i<=this.maxclients; i++){
      this.clients[clientId].connection.sendUTF(this.objects[i].getJSON())
    }
    this.sendScore();
  }

  physicsMove(message) {
    let json;
    //console.log(message.utf8Data);
    if (message.type === 'utf8') {
      try {
      json = JSON.parse(message.utf8Data);
      } catch (e) {
        console.log('This doesn\'t look like a valid JSON: ',
            message.utf8Data);
        return;
      }
      if(json){
        //console.log("json");
        if(json.id !== undefined){
          //console.log("move");
          this.move(json);
        }
      }
    }
  }

  connectClient(connection, clientId){
    const id = clientId;
    if(this.clients[id]&& this.clients[id].connection && this.clients[id].connection.close){
      console.log("Dropping old connection "+id);
      this.clients[id].connection.close();
    }
    connection.sendUTF(JSON.stringify({yourId:id}));
    for(let i=0; i<=this.maxclients; i++){
      connection.sendUTF(this.objects[i].getJSON())
    }
    
    this.clients[clientId] = {connection:connection};
    this.clients[clientId].connection.sendUTF(JSON.stringify({yourId:id}));
    for(let i=0; i<=this.maxclients; i++){
      this.clients[clientId].connection.sendUTF(this.objects[i].getJSON())
    }
    this.sendScore();
    const that = this;
    connection.on('message', function(message) {
      that.physicsMove(message);
    });
  }

  physicsClose(id){
    this.clients[id] = null;
    this.objects[id].move(-50,-50);
    this.move(this.objects[id]);
  }





  }

  export { Physics };
