const ai = class{
    constructor(objects, myId, width, height, move){
        this.myId = myId;
        this.objects =  objects;
        this.side = myId%2;
        this.width = width;
        this.height = height;
        this.move = move;
        this.running = true;
        this.objects[myId].x = width/3;
        this.objects[myId].y = height/2;
        const that = this;
        setInterval(() => {that.refresh()}, 50);
    }

    refresh(){

        if(this.running){
            const puck = this.objects[this.objects.length-2];
            const me = this.objects[this.myId];
            const target = {x:((this.side === 1)?this.width:0), y:this.height/2};
            const ownGoal = {x:((this.side === 0)?this.width:0), y:this.height/2};
            const dx = puck.x-me.x;
            const dy = puck.y-me.y;
            const len = Math.sqrt(dx*dx+dy*dy);
            const nlen = 0.005;
            if(len > 0.0001){
                const m = nlen/len;
                const move = { dx:dx*m,dy:dy*m};
                //console.log("move" +me.x)        
                if((me.x+move.dx <this.width/3 && this.side ==1) ||
                (me.x+move.dx >this.width*2/3 && this.side ==0) )
                {
                    me.x += move.dx;
                    me.y += move.dy;
                    this.move(me);
                }
            }
        }
        
    }
}



export {ai}
